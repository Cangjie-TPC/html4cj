# Changelog

## 1.0.0

1. 更新到cjc_0.58.3

## v0.0.1

仓颉版本的HTML解析

- 从字符串解析 HTML
- 使用 DOM 遍历或 CSS 选择器查找和提取数据
- 操作 HTML 元素、属性和文本
- 格式化HTML输出
- 支持设置Element对象的属性/相关节点
